# Gradle build plugins

This project contains a set of Gradle build plugins. It is intended to be used as a submodule of project which use Gradle as build system.

## Usage

Clone this repository in the directory `buildSrc/subprojects` of your Gradle project.

The directory `buildSrc` must contain a file `settings.gradle` containing the following lines:
```groovy
file("subprojects").eachDir { spDir ->
  logger.debug("Found Gradle build plugins subproject directory: ${spDir.absolutePath}")
  include(spDir.name)
  project(":${spDir.name}").projectDir = spDir
}
```

The directory `buildSrc` must contain a file `build.gradle` containing the following lines:
```groovy
dependencies { runtimeOnly subprojects }
```

You can now use the Gradle build plugins defined in this project.
