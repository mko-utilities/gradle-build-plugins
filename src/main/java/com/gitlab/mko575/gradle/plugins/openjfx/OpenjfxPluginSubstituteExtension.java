package com.gitlab.mko575.gradle.plugins.substitutes.openjfx;

import java.util.List;
import javax.inject.Inject;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;

/**
 * This defines the DSL for configuring the associated plugin, through the
 * definition of an extension for it.
 * @See
 * <a href="https://docs.gradle.org/current/userguide/custom_plugins.html#sec:getting_input_from_the_build">
 * Developing Custom Gradle Plugins: Making the plugin configurable</a>
 * @See
 * <a href="https://docs.gradle.org/current/userguide/implementing_gradle_plugins.html#modeling_dsl_like_apis">
 * Implementing Gradle plugins: Modeling DSL-like APIs</a>
 */
abstract public class OpenjfxPluginSubstituteExtension {

  private Property<String> version;

  /**
   * Reports the version of OpenJFX to use.
   * To determine which version to use:
   *   - check 'https://gluonhq.com/products/javafx/' to determine which major revision is the latest LTS one;
   *   - check which is the most recent version of this major revision available on the repositories you use:
   *      - for Maven Central check 'https://mvnrepository.com/artifact/org.openjfx/javafx'.
   */
  Property<String> getVersion() { return version; }

  private ListProperty<String> modules;

  /**
   * Reports the OpenJFX modules used by the project.
   */
  ListProperty<String> getModules() { return modules; };

  /**
   * Returns an extension object with the default values set.
   * @param objectFactory factory to create Property objects.
   */
  @Inject
  public OpenjfxPluginSubstituteExtension(final ObjectFactory objectFactory) {
    version = objectFactory.property(String.class).convention("17.0.2");
    modules = objectFactory.listProperty(String.class).convention(List.of( "base" ));
  }
}
