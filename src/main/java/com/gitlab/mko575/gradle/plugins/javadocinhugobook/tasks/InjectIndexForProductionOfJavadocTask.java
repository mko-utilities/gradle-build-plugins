package com.gitlab.mko575.gradle.plugins.javadocinhugobook.tasks;

import com.gitlab.mko575.gradle.plugins.javadocinhugobook.PublishSiblingsJavadocWithHugoBookExtension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import javax.inject.Inject;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.StopExecutionException;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.TaskExecutionException;
import org.gradle.api.tasks.javadoc.Javadoc;

/**
 * This task creates the Hugo Book index files for the javadoc created by a
 * specific {@link Javadoc} task.
 */
public abstract class InjectIndexForProductionOfJavadocTask extends DefaultTask {

  /**
   * The Javadoc task whose production gets injected into the Hugo Book documentation.
   */
  private final Javadoc jdTask;

  @OutputDirectory
  public abstract DirectoryProperty getOutputDir();

  /**
   * Create a task to inject the javadoc produced by the task given in parameter.
   * @param extension the extension object associated to this plugin application.
   * @param jdt the {@link Javadoc} Task whose production is to be injected.
   */
  @Inject
  public InjectIndexForProductionOfJavadocTask(
      PublishSiblingsJavadocWithHugoBookExtension extension,
      Javadoc jdt
  ) {
    jdTask = jdt;
    Path outputPath =
        Path.of(
            extension.getJavadocHugoSrcRootDir().get(),
            extension.getIdForJavadocTask(jdTask).toLowerCase(Locale.ROOT)
        );
    getOutputDir().set(getProject().file(outputPath));
  }

  @TaskAction
  public void process() {
    getLogger().lifecycle(
        String.format(
            "Processing index creation task for Javadoc task '%s' of %s."
                + " It will create indexes into %s.",
            jdTask.getName(),
            jdTask.getProject().getDisplayName(),
            getOutputDir().get()
        )
    );
    File indexFile = getOutputDir().get().file("_index.md").getAsFile();
    if (indexFile.isFile()) {
      throw new StopExecutionException(
          String.format("File %s already exists!", indexFile.getPath())
      );
    } else {
      String indexText =
          String.format("""
                  ---
                  bookCollapseSection: true
                  title: "%s"
                  weight: 10
                  ---

                  This section contains the Javadoc of module %s. The following entry points are available:
                  - [allclasses-index](allclasses-index.html)
                  - [allpackages-index](allpackages-index.html)
                  - [constant-values](constant-values.html)
                  - [help-doc](help-doc.html)
                  - [index](javadoc-index.html)
                  - [index-all](index-all.html)
                  - [overview-tree](overview-tree.html)
                  - [serialized-form](serialized-form.html)
                  """,
              jdTask.getProject().getName(),
              jdTask.getProject().getName()
          );
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(indexFile))) {
        writer.write(indexText);
      } catch (IOException e) {
        throw new TaskExecutionException(this, e);
      }
    }
  }

  /**
   * Returns the "canonical" task name for a task of this type for the provided
   * {@link Javadoc} task.
   * @param jdt the task whose index injection task name to provide.
   * @return the "canonical" name of the Javadoc indexes injection task.
   */
  public static String getTaskNameFor(Javadoc jdt) {
    String pName = jdt.getProject().getName();
    String tName = jdt.getName();
    return String.format(
        "injectIndexesFor%s%s%s%s",
        pName.substring(0,1).toUpperCase(Locale.ROOT),
        pName.substring(1),
        tName.substring(0,1).toUpperCase(Locale.ROOT),
        tName.substring(1)
    );
  }
}