package com.gitlab.mko575.gradle.plugins.substitutes.openjfx;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;

class OpenjfxPluginSubstitute implements Plugin<Project> {

    private static String OPENJFX_GROUP = "org.openjfx";

    private static Map<String, String> PLATFORM_MAPPINGS = Map.of(
        "linux"          , "linux",
        "linux-x86_64"   , "linux",
        "linux-aarch_64" , "linux",
        "mac"            , "mac",
        "osx-x86_64"     , "mac",
        "osx-aarch64"    , "mac-aarch64",
        "win"            , "win",
        "windows-x86_64" , "win"
    );

    private static Map<String, Set<String>> MODULE_DEPENDENCIES = Map.of(
        "base"     , Set.of(),
        "graphics" , Set.of("base"),
        "controls" , Set.of("base", "graphics"),
        "fxml"     , Set.of("base", "graphics"),
        "media"    , Set.of("base", "graphics"),
        "swing"    , Set.of("base", "graphics"),
        "web"      , Set.of("base", "graphics", "controls", "media")
    );

    @Override
    public void apply(Project project) {
        // Add the extension object
        OpenjfxPluginSubstituteExtension extension =
          project.getExtensions().create("javafx", OpenjfxPluginSubstituteExtension.class);

        // extension.configurations.all((c) -> {
        //     p.logger.lifecycle("Is this a configuration? ${c}");
        // });

      project.afterEvaluate(p -> {
            String openjfxVersion = extension.getVersion().get();
            Set<String> modulesUsed = new HashSet<>(extension.getModules().get());

            String osName = System.getProperty("os.name").toLowerCase();
            String openjfxPlatform = PLATFORM_MAPPINGS.getOrDefault(osName, null);
            p.getLogger().info(
              String.format(
                "OpenJFX platform to use: %s/%s/%s (knowing we are on %s)",
                OPENJFX_GROUP, openjfxVersion, openjfxPlatform, osName
                )
            );

            SortedSet<String> dependenciesToProcess = new TreeSet<>(modulesUsed);
            while ( ! dependenciesToProcess.isEmpty() ) {
                String processedModule = dependenciesToProcess.first();
                Set<String> addedDependencies =
                  MODULE_DEPENDENCIES.getOrDefault(processedModule, Set.of())
                    .stream()
                    .filter(m -> ! (modulesUsed.contains(m) || dependenciesToProcess.contains(m)))
                    .collect(Collectors.toSet());
                modulesUsed.addAll(addedDependencies);
                dependenciesToProcess.addAll(addedDependencies);
                dependenciesToProcess.remove(processedModule);
            }
            p.getLogger().info(
              String.format(
                "OpenJFX modules to load: %s",
                modulesUsed.stream()
                  .map(m -> String.format("javafx-%s", m))
                  .collect(Collectors.joining(", "))
              )
            );

            DependencyHandler dependencies = p.getDependencies();
            modulesUsed.stream()
              .map(m -> String.format("javafx-%s", m))
              .forEach(m -> {
                dependencies.add("implementation",
                  String.format(
                    "%s:%s:%s:%s", OPENJFX_GROUP, m, openjfxVersion, openjfxPlatform
                  )
                );
              });
        });
    }
}
