package com.gitlab.mko575.gradle.plugins.javadocinhugobook;

import com.gitlab.mko575.gradle.plugins.javadocinhugobook.tasks.InjectIndexForProductionOfJavadocTask;
import com.gitlab.mko575.gradle.plugins.javadocinhugobook.tasks.InjectMainJavadocIndex;
import java.io.File;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.file.Directory;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.javadoc.Javadoc;

/**
 * Plugin to inject Javadoc files generated from other subprojects into a Hugo Book subproject.
 */
public class PublishSiblingsJavadocWithHugoBook implements Plugin<Project> {

  /**
   * The name of the main task of this plugin.
   */
  public static final String MAIN_TASK_NAME = "injectJavadocToHugoBook";

  /**
   * The name of the general task in charge of the final assembly of all files.
   */
  protected static final String ASSEMBLE_TASK_NAME = "assembleHugoSrcWithJavadoc";

  /**
   * The name of the task inject injecting the main Javadoc index.
   */
  protected static final String MAIN_INDEX_TASK_NAME = "injectMainJavadocIndex";

  /**
   * Configures the main task of this plugin.
   * @param extension the extension object associated to this plugin application.
   * @param project the project on which the plugin is applied.
   * @param taskProvider the provider of the task to configure.
   * @return the provider of the main task of this plugin (see
   *  {@link PublishSiblingsJavadocWithHugoBook#MAIN_TASK_NAME})
   */
  private void configureMainTask(
      PublishSiblingsJavadocWithHugoBookExtension extension,
      Project project,
      TaskProvider<Task> taskProvider
  ) {
    Task task = taskProvider.get();
    task.setGroup("Documentation");
    task.setDescription("Injects the generated project javadocs into the Hugo Book documentation.");
    String relOutputPath = extension.getAssembledHugoWithJavadocSrcDir().get();
    Provider<Directory> outputDir = project.getLayout().getBuildDirectory().dir(relOutputPath);
    task.getOutputs()
        .dir(outputDir)
        .withPropertyName("outputDir");
  }

  /**
   * Creates, registers and returns the provider of the final assembly task (see
   * {@link PublishSiblingsJavadocWithHugoBook#ASSEMBLE_TASK_NAME} for its name).
   * @param extension the extension object associated to this plugin application.
   * @param project the project on which the plugin is applied.
   * @return the provider of the final assembly task.
   */
  private TaskProvider<Copy> createTaskFinalAssembly(
      PublishSiblingsJavadocWithHugoBookExtension extension,
      Project project
  ) {
    TaskProvider<Copy> taskProvider =
        project.getTasks().register(ASSEMBLE_TASK_NAME, Copy.class);
    Copy task = taskProvider.get();
    task.setDescription(
        "Assembles Hugo sources files and Javadoc generated files in a single directory."
    );
    File preAssembledHugoSrcDir =
        project.file(extension.getPreAssembledHugoSrcDir().get());
    File assembledJavadocFilesDir =
        project.file(extension.getCopiedJavadocFilesDir());
    File assembledHugoWithJavadocSrcDir =
        project.file(extension.getAssembledHugoWithJavadocSrcDir());
    task.from(preAssembledHugoSrcDir);
    task.from(assembledJavadocFilesDir);
    task.into(assembledHugoWithJavadocSrcDir);
    return taskProvider;
  }

  /**
   * Creates, registers and returns the provider of a task that copies files created
   * by the provided {@link Javadoc} task into the temporary Javadoc files temporary
   * assembly directory.
   * @param extension the extension object associated to this plugin application.
   * @param project the project on which the plugin is applied.
   * @param javadocTask the task whose production is to be copied.
   * @return the provider of the created task.
   */
  private TaskProvider<Copy> createTaskCopyJavadocFilesOfTask(
      PublishSiblingsJavadocWithHugoBookExtension extension,
      Project project,
      Javadoc javadocTask
  ) {
    Project sp = javadocTask.getProject();

    // Computing from and to directories
    Path srcDir = Path.of(javadocTask.getOutputs().getFiles().getAsPath());
    Path destDir =
        Path.of(
            extension.getAssembledJavadocGeneratedFilesRootDir().get(),
            sp.getName().toLowerCase(Locale.ROOT)
        );

    // Creating the copy task
    String newTaskNameSuffix =
        String.format(
            "%s%s%s%s",
            sp.getName().substring(0,1).toUpperCase(Locale.ROOT),
            sp.getName().substring(1),
            javadocTask.getName().substring(0,1).toUpperCase(Locale.ROOT),
            javadocTask.getName().substring(1)
        );
    TaskProvider<Copy> copyJavadocTP =
        project.getTasks().register(
            String.format("extractJavadocFilesFrom%s", newTaskNameSuffix),
            Copy.class
        );
    Copy copyJavadocSubtask = copyJavadocTP.get();
    copyJavadocSubtask.setDescription(
        String.format(
            "Extract Javadoc files generated by task '%s' of %s",
            javadocTask.getName(), sp.getDisplayName()
        )
    );
    copyJavadocSubtask.dependsOn(javadocTask);
    copyJavadocSubtask
        .from(srcDir)
        .rename("^index.html$", "javadoc-index.html");
    copyJavadocSubtask
        .into(destDir);
    return copyJavadocTP;
  }

  /**
   * 'Temporary' function transitively collecting all (sub-){@link Project} of
   * a given {@link Project}. {@link Project#getAllprojects()} may be equivalent;
   * however it is not clear if this method is transitive or not.
   * If {@link Project#getAllprojects()} is found equivalent, calls to this
   * function should be replaced by it. Otherwise, if it is found not equivalent,
   * the assert statement should be removed.
   * @param rootProject the project form which to collect all projects.
   * @return the transitive set of all projects of the provided project.
   */
  private static Set<Project> transitivelyGetProjectsOf(Project rootProject) {
    Set<Project> res =
        rootProject.getSubprojects().stream().collect(
            () -> new HashSet<>(Set.of(rootProject)),
            (Set<Project> acc, Project p) -> acc.addAll(transitivelyGetProjectsOf(p)),
            Set::addAll
        );
    // It is not clear if Project.getAllprojects() is transitive or not ...
    assert res == rootProject.getAllprojects();
    return res;
  }

  @Override
  public void apply(Project project) {
    // Add the extension object
    PublishSiblingsJavadocWithHugoBookExtension extension =
        project.getExtensions().create(
            MAIN_TASK_NAME,
            PublishSiblingsJavadocWithHugoBookExtension.class
        );

    TaskProvider<Task> mainTaskP =
        project.getTasks().register(MAIN_TASK_NAME);

    project.afterEvaluate(p -> {
      project.getLogger().debug(extension.propertiesToString());

      // Create final assembly task
      TaskProvider<Copy> finalAssemblyTaskP =
          createTaskFinalAssembly(extension, p);

      // Create main task
      configureMainTask(extension, p, mainTaskP);
      mainTaskP.get().dependsOn(finalAssemblyTaskP);

      // Create needed task for every Javadoc tasks in the root project
      Set<Javadoc> allJavadocTasks =
          transitivelyGetProjectsOf(p.getRootProject()).stream()
              .flatMap(sp -> sp.getTasks().withType(Javadoc.class).stream())
              .collect(Collectors.toSet());
      allJavadocTasks
          .forEach( (Javadoc jdt) -> {
            project.getLogger().debug(
                String.format(
                    "Plugin applied to %s found the Javadoc task '%s' in %s",
                    project.getDisplayName(),
                    jdt.getName(), jdt.getProject().getDisplayName()
                )
            );

            // Create the task to inject the index for this javadoc
            TaskProvider<InjectIndexForProductionOfJavadocTask> createIndexesTaskP =
                project.getTasks().register(
                    InjectIndexForProductionOfJavadocTask.getTaskNameFor(jdt),
                    InjectIndexForProductionOfJavadocTask.class,
                    extension, jdt
                );
            finalAssemblyTaskP.get().dependsOn(createIndexesTaskP);

            // Create the task to copy the Javadoc files for this javadoc
            TaskProvider<Copy> copyJavadocFilesTaskP =
                createTaskCopyJavadocFilesOfTask(extension, p, jdt);
            finalAssemblyTaskP.get().dependsOn(copyJavadocFilesTaskP);
          });

      // Creating the task for the main index
      TaskProvider<InjectMainJavadocIndex> injectMainJavadocIndexTaskP =
          project.getTasks().register(
              MAIN_INDEX_TASK_NAME,
              InjectMainJavadocIndex.class,
              extension, allJavadocTasks
          );
      finalAssemblyTaskP.get().dependsOn(injectMainJavadocIndexTaskP);
    });
  }
}