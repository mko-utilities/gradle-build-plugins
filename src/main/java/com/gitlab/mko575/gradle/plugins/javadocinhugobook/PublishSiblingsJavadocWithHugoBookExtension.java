package com.gitlab.mko575.gradle.plugins.javadocinhugobook;

import java.util.Locale;
import javax.inject.Inject;
import org.gradle.api.Project;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.tasks.javadoc.Javadoc;

/**
 * This defines the DSL for configuring the associated plugin, through the
 * definition of an extension for it.
 * @See
 * <a href="https://docs.gradle.org/current/userguide/custom_plugins.html#sec:getting_input_from_the_build">
 * Developing Custom Gradle Plugins: Making the plugin configurable</a>
 * @See
 * <a href="https://docs.gradle.org/current/userguide/implementing_gradle_plugins.html#modeling_dsl_like_apis">
 * Implementing Gradle plugins: Modeling DSL-like APIs</a>
 */
public abstract class PublishSiblingsJavadocWithHugoBookExtension {

  /* ********************************************************************* */
  /* * ROOT PROPERTIES
  /* ********************************************************************* */
  
  /**
   * Reports the directory into which original user-created Hugo sources are stored
   */
  public Property<String> getOriginalHugoSrcDir() { return originalHugoSrcDir; }
  private final Property<String> originalHugoSrcDir;

  /**
   * Reports the directory into which Hugo sources are stored before final
   * assembling by Javadoc injection.
   */
  public Property<String> getPreAssembledHugoSrcDir() { return preAssembledHugoSrcDir; }
  private final Property<String> preAssembledHugoSrcDir;

  /**
   * Reports the directory into which Javadoc generated files get copied before assembly.
   */
  public Property<String> getCopiedJavadocFilesDir() { return copiedJavadocFilesDir; }
  private final Property<String> copiedJavadocFilesDir;

  /**
   * Reports the directory into which assembled Hugo sources with Javadoc are stored
   */
  public Property<String> getAssembledHugoWithJavadocSrcDir() {
    return assembledHugoWithJavadocSrcDir;
  }
  private final Property<String> assembledHugoWithJavadocSrcDir;

  /**
   * Reports the path infix for content sources processed by Hugo
   */
  public Property<String> getContentFilesPathInfix() { return contentFilesPathInfix; }
  private final Property<String> contentFilesPathInfix;

  /**
   * Reports the path infix for static content not processed by Hugo
   */
  public Property<String> getStaticFilesPathInfix() { return staticFilesPathInfix; }
  private final Property<String> staticFilesPathInfix;

  /**
   * Reports the website URL infix for javadoc content
   */
  public Property<String> getJavadocUrlInfix() { return javadocUrlInfix; }
  private final Property<String> javadocUrlInfix;
  
  /* ********************************************************************* */
  /* * COMPUTED PROPERTIES
  /* ********************************************************************* */
  
  /**
   * Reports the root directory where to create new Markdown files related to
   * Javadoc documentation.
   */
  public Property<String> getJavadocHugoSrcRootDir() { return javadocHugoSrcRootDir; }
  private final Property<String> javadocHugoSrcRootDir;

  /**
   * Computes, from the other properties of this extension, the path of the
   * root directory where to create new Markdown files related to Javadoc documentation.
   * @return the computed path of the directory where to create new Markdown files.
   */
  private String computeJavadocHugoSrcRootDir() {
    return String.format("%s/%s/%s",
      getOriginalHugoSrcDir().get(),
      getContentFilesPathInfix().get(),
      getJavadocUrlInfix().get()
    );
  }

  /**
   * Reports the root directory where are temporarily assembled Javadoc generated
   * files.
   */
  public Property<String> getAssembledJavadocGeneratedFilesRootDir() { return assembledJavadocGeneratedFilesRootDir; }
  private final Property<String> assembledJavadocGeneratedFilesRootDir;

  /**
   * Computes, from the other properties of this extension, the path of the
   * root directory where are temporarily assembled Javadoc generated files.
   * @return the computed path of the directory where are assembled Javadoc
   *  generated files.
   */
  private String computeAssembledJavadocGeneratedFilesRootDir() {
    return String.format("%s/%s/%s",
        getCopiedJavadocFilesDir().get(),
        getStaticFilesPathInfix().get(),
        getJavadocUrlInfix().get()
    );
  }

  /* ********************************************************************* */
  /* * CONSTRUCTOR
  /* ********************************************************************* */
  
  /**
   * Returns an extension object with the default values set.
   * @param objectFactory factory to create {@link Property} objects.
   * @param providerFactory factory to create {@link Provider} objects.
   */
  // This method needs to be public for 'Project.getExtensions().create' to
  // work; even if SonarLint says otherwise.
  @Inject
  public PublishSiblingsJavadocWithHugoBookExtension(
      final ObjectFactory objectFactory, final ProviderFactory providerFactory
  ) {
    originalHugoSrcDir =
        objectFactory.property(String.class).convention("src/hugo");
    preAssembledHugoSrcDir =
        objectFactory.property(String.class).convention(getOriginalHugoSrcDir());
    copiedJavadocFilesDir =
        objectFactory.property(String.class).convention("build/tmp/sources/hugoJavadocFiles");
    assembledHugoWithJavadocSrcDir =
        objectFactory.property(String.class).convention("build/generated/sources/hugo");
    contentFilesPathInfix =
        objectFactory.property(String.class).convention("content");
    staticFilesPathInfix =
        objectFactory.property(String.class).convention("static");
    javadocUrlInfix =
        objectFactory.property(String.class).convention("docs/javadoc");
    javadocHugoSrcRootDir =
        objectFactory.property(String.class).convention(
            providerFactory.provider(this::computeJavadocHugoSrcRootDir)
        );
    assembledJavadocGeneratedFilesRootDir =
        objectFactory.property(String.class).convention(
            providerFactory.provider(this::computeAssembledJavadocGeneratedFilesRootDir)
        );
  }

  /* ********************************************************************* */
  /* * Helper functions
  /* ********************************************************************* */

  /**
   * Returns a unique Id for this {@link Javadoc} task. The Id is unique among
   * all {@link Javadoc} tasks, but not among all tasks.
   * @param jdt the task whose unique Id to retrieve.
   * @return a unique Id for this task.
   */
  public String getIdForJavadocTask(Javadoc jdt) {
    Project p = jdt.getProject();
    String pId = String.format(
        "%s%s",
        p.getName().substring(0,1).toUpperCase(Locale.ROOT),
        p.getName().substring(1)
      );
    if ( p.getTasks().withType(Javadoc.class).size() > 1 ) {
      return String.format(
        "%s%s%s", pId,
        jdt.getName().substring(0,1).toUpperCase(Locale.ROOT),
        jdt.getName().substring(1)
      );
    } else {
      return pId;
    }
  }

  /**
   * Returns a {@link String} describing the current values of all properties
   * defined in this extension.
   * @return a {@link String} describing the current values of this extension's
   *  properties
   */
  public String propertiesToString() {
    return String.format(
        "\noriginalHugoSrcDir = %s\n"
            + "preAssembledHugoSrcDir = %s\n"
            + "copiedJavadocFilesDir = %s\n"
            + "assembledHugoWithJavadocSrcDir = %s\n"
            + "contentFilesPathInfix = %s\n"
            + "staticFilesPathInfix = %s\n"
            + "javadocUrlInfix = %s\n"
            + "javadocHugoSrcRootDir = %s\n"
            + "assembledJavadocGeneratedFilesRootDir = %s\n",
        getOriginalHugoSrcDir().get(),
        getPreAssembledHugoSrcDir().get(),
        getCopiedJavadocFilesDir().get(),
        getAssembledHugoWithJavadocSrcDir().get(),
        getContentFilesPathInfix().get(),
        getStaticFilesPathInfix().get(),
        getJavadocUrlInfix().get(),
        getJavadocHugoSrcRootDir().get(),
        getAssembledJavadocGeneratedFilesRootDir().get()
    );
  }

}