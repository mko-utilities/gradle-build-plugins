package com.gitlab.mko575.gradle.plugins.javadocinhugobook.tasks;

import com.gitlab.mko575.gradle.plugins.javadocinhugobook.PublishSiblingsJavadocWithHugoBookExtension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.StopExecutionException;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.TaskExecutionException;
import org.gradle.api.tasks.javadoc.Javadoc;

public abstract class InjectMainJavadocIndex extends DefaultTask {

  /**
   * The extension object associated to this plugin application.
   */
  private final PublishSiblingsJavadocWithHugoBookExtension extension;

  /**
   * The Javadoc tasks which are injected into the documentation.
   */
  private final Set<Javadoc> documentedJavadocTasks;

  @OutputDirectory
  public abstract DirectoryProperty getOutputDir();

  /**
   * Create a task to inject the javadoc produced by the task given in parameter.
   * @param extension the extension object associated to this plugin application.
   * @param jdTasks the {@link Javadoc} Tasks which are documented.
   */
  @Inject
  public InjectMainJavadocIndex (
      PublishSiblingsJavadocWithHugoBookExtension extension,
      Set<Javadoc> jdTasks
  ) {
    this.extension = extension;
    documentedJavadocTasks = jdTasks;
    Path outputPath =
        Path.of(extension.getJavadocHugoSrcRootDir().get());
    getOutputDir().set(getProject().file(outputPath));
  }

  @TaskAction
  public void process() {
    getLogger().lifecycle(
        String.format(
            "Processing main Javadoc index creation task."
                + " It will create indexes into %s.",
            getOutputDir().get()
        )
    );
    File indexFile = getOutputDir().get().file("_index.md").getAsFile();
    if (indexFile.isFile()) {
      throw new StopExecutionException(
          String.format("File %s already exists!", indexFile.getPath())
      );
    } else {
      String indexText =
          String.format("""
                  ---
                  bookCollapseSection: true
                  weight: 100
                  title: "Javadoc"
                  bookFlatSection: false
                  ---

                  This section contains the Javadoc of %s.
                  """,
              documentedJavadocTasks.stream().map(
                  extension::getIdForJavadocTask
              ).collect(Collectors.joining(", "))
          );
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(indexFile))) {
        writer.write(indexText);
      } catch (IOException e) {
        throw new TaskExecutionException(this, e);
      }
    }
  }

}
